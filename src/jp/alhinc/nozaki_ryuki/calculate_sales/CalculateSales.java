package jp.alhinc.nozaki_ryuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Filter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		if (name.matches("[0-9]{8}.rcd")) {
			return true;

		} else {
			return false;
		}
	}
}

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> map = new HashMap<String, String>();

		try {
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.print("支店定義ファイルが存在しません");
				return;
			}

			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				String date;
				while ((date = br.readLine()) != null) {
					String[] bo = date.split(",");
					if (bo.length != 2 || !bo[0].matches("[0-9]{3}")) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map.put(bo[0], bo[1]);
				}
			} finally {
				br.close();
			}

			File dir = new File(args[0]);
			File[] files = dir.listFiles(new Filter());

			for (int i = 0; i < files.length - 1; i++) {
				int start = Integer.parseInt(files[i].getName().substring(0, 8));
				int end = (Integer.parseInt(files[i + 1].getName().substring(0, 8)));
				if (end - start != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			List<String> lineList = new ArrayList<String>();
			List<String> lineList2 = new ArrayList<String>();
			BufferedReader br2 = null;
			try {
				String date2;
				for (int i = 0; i < files.length; i++) {
					br2 = new BufferedReader(new FileReader(files[i]));
					while ((date2 = br2.readLine()) != null) {
						lineList.add(date2);
						date2 = br2.readLine();
						lineList2.add(date2);
						if ((date2 = br2.readLine()) != null) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}
					}
				}
			} finally {
				br2.close();
			}

			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));
			HashMap<String, Long> map2 = new HashMap<String, Long>();
			try {
				for (int i = 0; i < lineList.size(); i++) {
					if (!map.containsKey(lineList.get(i))) {
						System.out.println(files[i].getName() + "の支店コードが不正です");
						return;
					} else {
						if (map2.get(lineList.get(i)) == null) {
							map2.put(lineList.get(i), Long.parseLong(lineList2.get(i)));
						} else {
							map2.put(lineList.get(i), map2.get(lineList.get(i)) + Long.parseLong(lineList2.get(i)));
							if (map2.get(lineList.get(i)) > 9999999999L) {
								System.out.println("合計金額が10桁を超えました");
								return;
							}
						}
					}
				}
				for (String key : map.keySet()) {
					try (BufferedReader reader = new BufferedReader(new StringReader(key))) {
						String line = null;
						while ((line = reader.readLine()) != null) {
							if (!lineList.contains(line)) {
								map.put(line, line + "," + map.get(line) + "," + 0 + "\r\n");
							} else {
								map.put(line, line + "," + map.get(line) + "," + map2.get(line) + "\r\n");
							}
						}
					}
				}
				for (String val : map.values()) {
					pw.print(val);
				}
			} finally {
				pw.close();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}